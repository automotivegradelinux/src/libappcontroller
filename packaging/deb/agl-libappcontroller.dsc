Format: 1.0
Source: agl-libappcontroller
Binary: agl-libappcontroller-bin, agl-libappcontroller-dev
Architecture: any
Version: 2.0-0
Maintainer: romain.forlot <romain.forlot@iot.bzh>
Standards-Version: 3.8.2
Homepage: https://gerrit.automotivelinux.org/gerrit/src/libappcontroller
Build-Depends: debhelper (>= 5),
 cmake,
 dpkg-dev,
 pkg-config,
 liblua5.3-dev,
 libjson-c-dev,
 libsystemd-dev,
 agl-libafb-helpers-dev,
 agl-app-framework-binder-bin,
 agl-app-framework-binder-dev,
DEBTRANSFORM-RELEASE: 1
Files:
 libappcontroller_2.0.tar.gz
